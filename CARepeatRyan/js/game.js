var canvas,
ctx,
x,
y,
touchHappened,
mobile = false,
width = 600,
height = 600,
arenaSize,
awidth,
aheight,
DESKTOP_ARENA_WIDTH,
DESKTOP_ARENA_HEIGHT,
arenaHeight,
arenaWidth,
mWidth,
mHeight,
player_x=(width/2) -25, player_y = height -75, player_w = 50, player_h = 50,
score = 0,
alive = true,
victory =false,
victoryStart = false,
life = 3,
direction = 1,
invulnerableStart,
invulnerable,
invulnerableDuration = 2000,
roundWillEnd,
score, highScore = 0,
scoreModifier = 10;
moving = false,
fireing = false,
start = true,
eStart = true,
gameStart = true;
pAnimationPos =0,
eAnimationPos =0;
hit = false;

highScore = localStorage.getItem("HighScore");//gets high score from local storage
    if (highScore === null)
    {
        highScore = 0;
    }
//Related to HTML
var         highScoreElement = document.getElementById('highScore'),
            fpsElement = document.getElementById('fps'),
            scoreElement = document.getElementById('score'),
            gameTimeElapsedElement = document.getElementById('gameTimeElapsed'),
            soundAndMusicElement = document.getElementById('sound-and-music'),
            toastElement = document.getElementById('toast');

//Music and sounds effects
var song = document.getElementById("music");
var shoot = document.getElementById("shoot");
var enemyDead = document.getElementById("enemyDead");
var playerDamage = document.getElementById("playerDam");
var transition = document.getElementById("levelTransition");

// Time Related variables
var gameTimeElapsed = 0, 
animationTimer, shotTimer, shotFired, enemyAnimationTimer, invulnerablityTimer, roundTimer,gameTimer, victoryAnimationTimer, minFinal, opening;
opening = new Stopwatch()
gameTimer = new Stopwatch()
roundTimer = new Stopwatch()
shotTimer = new Stopwatch()
animationTimer = new Stopwatch()
enemyAnimationTimer = new Stopwatch()
invulnerablityTimer= new Stopwatch()
victoryAnimationTimer = new Stopwatch()

//sprites
var mutant, 
player;
var sprites = new Image();
 sprites.addEventListener("load", function ()
    {   staSprite = new Sprite(this, 21,25,41, 27);
    	newSprite = new Sprite(this, 21, 25, 41, 27);
    	enemySprite = new Sprite(this,432, 166, 47,26);

        playerNorth = [
            new Sprite(this, 18, 152, 41, 47), new Sprite(this, 17, 79, 38, 51),
            new Sprite(this, 21, 25, 41, 27), new Sprite(this, 74, 24, 40, 47),
            new Sprite(this, 77, 142, 38, 50), new Sprite(this, 71, 91, 40, 27), 
            new Sprite(this, 19, 214, 30, 45)
               ];
       playerSouth = [
            new Sprite(this, 144, 85, 41, 47), new Sprite(this, 143, 154, 38, 51),
            new Sprite(this, 147, 232, 41, 27), new Sprite(this, 200, 213, 40, 47),
            new Sprite(this, 203, 97, 38, 50), new Sprite(this, 197, 166, 40, 27),
            new Sprite(this, 145, 25, 30, 45)
            ];
       playerWest = [
            new Sprite(this, 144, 271, 47, 41), new Sprite(this, 72, 269, 51, 38),
            new Sprite(this, 17, 273, 27 , 41), new Sprite(this, 16, 326, 47, 40),
            new Sprite(this, 134, 329, 50, 38), new Sprite(this, 83, 322, 27, 40),
            new Sprite(this, 206, 271, 45, 30)
            ];
       playerEast = [
            new Sprite(this, 76, 375, 47, 41),  new Sprite(this, 145, 373, 51, 38),
            new Sprite(this, 223, 377, 27 , 41), new Sprite(this, 83, 433, 47, 40),
            new Sprite(this, 157, 427, 27, 40), new Sprite(this, 204, 430, 50, 38),
            new Sprite(this, 16, 375, 45, 30)
            ];


        mutantNorth =[
            new Sprite(this, 432, 252, 43, 42),  new Sprite(this, 435, 204, 44, 43),
            new Sprite(this, 432, 166, 47, 26)];
        mutantSouth =[
            new Sprite(this, 485, 158, 43, 42),  new Sprite(this, 487, 205, 44, 43),
            new Sprite(this, 484, 260, 47, 26)];
        mutantWest =[
            new Sprite(this, 516, 103, 42, 43),  new Sprite(this, 468, 107, 43, 44),
            new Sprite(this, 430, 104, 26, 47)];
        mutantEast =[
            new Sprite(this, 419, 46, 42, 43),  new Sprite(this, 467, 51, 43, 44),
            new Sprite(this, 522, 48, 26, 47)];
        hearts = new Sprite(this, 737, 63, 46, 50);
        blood = new Sprite(this, 647, 60,59, 47);
        gunSprite = new Sprite(this, 24, 539, 146, 106);
        dPadSprite = new Sprite(this, 208, 480, 190, 200)
    });
 sprites.src = "Sprites/SpriteSheet.png";

// BackGround image
backGround = new Image();
backGround.src = 'Sprites/BackGround.png',
bgx = 0, bgy = 0;

//keys to control player
rightKey = false,
leftKey= false,
upKey = false,
downKey =false;

//Our Enemies
enemyTotal = 5,
enemies = [],
enemy_w = 50,
enemy_h = 50,
speed = 3,
enemyDirection=  0;

//our bullets
var shots = 3,
bullets = [];

//our hearts
health = [];
health_x = 0,
health_y = 0,
health_w = 50,
health_h = 50
for(var i = 0; i < life; i++)
{
	health.push([health_x, health_y, health_w, health_h]);
	health_x += health_w + 2;
}
// blood stains left by enemies
bloodDrop = [];
bloodw = 59, bloodh = 47;

function detectMobile(){
   mobile = 'ontouchstart' in window;
}

function clearCanvas(){
	ctx.clearRect(0,0,width,height);
}
function playerControls()
{
	// player movement
	var preX = player_x, preY = player_y;// holds the previous position
	if (rightKey) 
	{
		player_x +=4;
		direction = 4;
	}
	else if (leftKey)
	{
	    player_x -= 4;
	    direction = 3;
	}
	if (upKey)
	{ 
		player_y -= 4;
		direction = 1;
	}
	else if (downKey)
	{
	    player_y += 4;
	    direction = 2;
	}

	if ((preX + preY) != (player_x + player_y))
	{
		// if there is any difference between the x and y alues from their previous states it animates the sprite
	 moving = true;
	}
	if(!rightKey && !leftKey && !upKey && !downKey)
	{
		moving = false;
	}
	// Keeps player on the canvas
	if(player_x <=0) player_x =0;
	if((player_x + player_w) >= width) player_x = width -player_w;
	if(player_y <= 0) player_y = 0;
	if((player_y + player_h) >= height) player_y = height - player_h;
     
}
function drawplayer()
{
	if(!moving && !fireing)
		{
			if(alive)
			{
			playerStationarySprite();
			DrawSprite(staSprite, player_x, player_y);
		    }
		    else if (!alive)
		    {
		    deadPlayerStationaryFactory();
			DrawSprite(staSprite, player_x, player_y);
		    }
		}

	playerControls();
    if (start)
    {
    	animationTimer.start()
    	start = false;
    }
       if(fireing && alive)
    { 
        playerShootinDirectionFactory();
    	DrawSprite(newSprite, player_x, player_y);
    	moving = false;
    	if(shotFired)
    	{ 
    	shotTimer.start();
    	shotFired = false;
        }	
    	if (shotTimer.getElapsedTime() > 500) fireing = false;
    }
    else if(fireing && !alive) fireing = false;
    else if(moving)
    {
    DrawSprite (newSprite, player_x, player_y);
    }
    if(animationTimer.getElapsedTime() > 200)
    {    
    cyclePlayer();
  
    start = true;
    }
}
//Cycles through the animaption position the activates the player direction Factory
function cyclePlayer()
{
  if(alive)
  {
   pAnimationPos++;
    if (pAnimationPos > 5)
    {
    	pAnimationPos=0;
    }
    playerDirectionFactory();
   }
   else if (!alive)
   	{
   		pAnimationPos++;
   		if(pAnimationPos > 2)
   		{
   			pAnimationPos = 0;
   		}
   		deadPlayerDirectionFactory();  
   	}
}
// updates the sprite to be drawn on to the canvas
function playerDirectionFactory()
{
	if(direction == 1) newSprite = playerNorth[pAnimationPos];
	if(direction == 2) newSprite = playerSouth[pAnimationPos];
	if(direction == 3) newSprite = playerWest[pAnimationPos];
	if(direction == 4) newSprite = playerEast[pAnimationPos];
}
//updates the direction of the shooting animation
function playerShootinDirectionFactory()
{
	if(direction == 1) newSprite = playerNorth[playerNorth.length - 1];
	if(direction == 2) newSprite = playerSouth[playerNorth.length - 1];
	if(direction == 3) newSprite = playerWest[playerNorth.length - 1];
	if(direction == 4) newSprite = playerEast[playerNorth.length - 1];
}
function playerStationarySprite()
{
	//gives the player a pose in the direction they are stading in when not moving 
	if(direction == 1) staSprite = playerNorth[2];
	if(direction == 2) staSprite = playerSouth[2];
	if(direction == 3) staSprite = playerWest[2];
	if(direction == 4) staSprite = playerEast[2];
}
function deadPlayerDirectionFactory()
{
	if(direction == 1) newSprite = mutantNorth[pAnimationPos];
	if(direction == 2) newSprite = mutantSouth[pAnimationPos];
	if(direction == 3) newSprite = mutantWest[pAnimationPos];
	if(direction == 4) newSprite = mutantEast[pAnimationPos];
}
function deadPlayerStationaryFactory()
{
	//gives the player a pose in the direction they are stading in when not moving 
	if(direction == 1) staSprite = mutantNorth[2];
	if(direction == 2) staSprite = mutantSouth[2];
	if(direction == 3) staSprite = mutantWest[2];
	if(direction == 4) staSprite = mutantEast[2];
}
//draws the health on screen
function drawHearts()
{
    for (var i = 0; i < health.length; i++)
	{
		DrawSprite (hearts, health[i][0], health[i][1]);
	}
}

function drawMobileIcons()
{
	DrawSprite(dPadSprite, 20, (height - 220)); 
	DrawSprite(gunSprite, width - 166, (height -160) )
}
function enemyCreation()
{
	for(var i = 0; i < enemyTotal; i++)
    {
    	var eAnimationFrame = 0;
    	var eDirection = Math.floor(Math.random() * 4) + 1;
    	if(eDirection > 4) eDirection = 1;
    	//Taken directly from http://atomicrobotdesign.com/blog/htmlcss/build-a-vertical-scrolling-shooter-game-with-html5-canvas-part-1/
	    enemies.push([(Math.random() * 500) + 50, Math.floor(Math.random() * 500 + 50) , enemy_w, enemy_h, Math.floor(Math.random() * 2)+ speed, eDirection, eAnimationFrame, enemySprite]);
    }
    gameStart = false,  roundWillEnd = true, invulnerableStart = true;
}
function drawEnemies()
{

    if (eStart)
    {
    	enemyAnimationTimer.start()
    	eStart = false;
    }
     
   
    if(enemyAnimationTimer .getElapsedTime() > 200)
    {  
    for (var i = 0; i < enemies.length; i++)
	{  
    cycleEnemies(i);
    }
    eStart = true;
    }
	for (var i = 0; i < enemies.length; i++)
	{
		//ctx.drawImage(mutant, enemies[i][0], enemies[i][1]);
		DrawSprite (enemies[i][7], enemies[i][0], enemies[i][1]);
	}
}
function cycleEnemies(i)
{
   enemies[i][6]++;
    if (enemies[i][6] > 2)
    {
    	enemies[i][6] = 0;
    }
    enemyDirectionFactory(i);

}
function enemyDirectionFactory(i)
{
	var enemyDirectionControl = enemies[i][5];
	if(enemyDirectionControl == 2) enemies[i][7] = mutantNorth[enemies[i][6]];
	if(enemyDirectionControl == 1) enemies[i][7] = mutantSouth[enemies[i][6]];
	if(enemyDirectionControl == 3) enemies[i][7] = mutantWest[enemies[i][6]];
	if(enemyDirectionControl == 4) enemies[i][7] = mutantEast[enemies[i][6]];
}
function moveEnemies()
{
	for(var i = 0; i < enemies.length; i++)
	{
		enemiesDirectionMovement(i);
	}
}
// controls Enemy Movements, when they walk to the edge of the screen they walk right back
function enemiesDirectionMovement(i)
{
	var enemyDirectionControl = enemies[i][5];
	// holds the bullet's direction so that there is no error when splicing the array
	                                           // this all controls the trajectory of the bullets
	if(enemyDirectionControl == 1)
	   {
 if (enemies[i][1] <= height)
		{
			enemies[i][1] += enemies[i][4];
		}
		else if (enemies[i][1] >= height)
		{
            enemies[i][5] = 2;

		}
	    }
	if(enemyDirectionControl == 2)
	    {
 if (enemies[i][1] >= -1)
		{
			enemies[i][1] -= enemies[i][4];
		}
		else if (enemies[i][1] <= -1)
		{
            enemies[i][5] = 1;
		}
	    }
	if(enemyDirectionControl == 3)
	    {
 if (enemies[i][0] >=-1)
		{
			enemies[i][0] -= enemies[i][4];
		}
		else if (enemies[i][0] <= -1)
		{
            enemies[i][5] = 4;
		}
	    }
	if(enemyDirectionControl == 4)
	{
 if (enemies[i][0] <= width)
		{
			enemies[i][0] += enemies[i][4];
		}
		else if (enemies[i][0] >= width - 1)
		{
            enemies[i][5] = 3;
		}
	}
}
//Colours in the bullets
function drawBullets() // creates bullets
{
	if (bullets.length)
		for (var i =0; i < bullets.length; i++)
		{
			ctx.fillStyle = '#fff';
			ctx.fillRect(bullets[i][0], bullets[i][1] ,bullets[i][2],bullets[i][3], bullets[i][4],bullets[i][5])
		}
}
//Passess on indvidual bullets to be accelerated by the direction factory for bullets
function moveBullets() {
	for (var i = 0; i < bullets.length; i++)
	{
		bulletDirectionFactory(i);
	}
}
function bulletDirectionFactory(i)
{
	var bulletDirectionControl = bullets[i][4];// holds the bullet's direction so that there is no error when splicing the array
	// this all controls the trajectory of the bullets
   // console.log(bullets[i][4]);
	if(bulletDirectionControl == 1)
	{
    if(bullets[i][1] >= -11) 
		{
			bullets[i][1] -= 10;
		}
		else if (bullets[i][1] <= -10)
	    {
			bullets.splice(i,1);
		}
	}
	if(bulletDirectionControl == 2)
	{
    if(bullets[i][1] <= 620) 
		{
			bullets[i][1] += 10;
		}
		else if (bullets[i][1] >= 620)
	    {
			bullets.splice(i,1);
		}
	}
	if(bulletDirectionControl == 3)
	{
    if(bullets[i][0] >= -11) 
		{
			bullets[i][0] -= 10;
		}
		else if (bullets[i][0] <= -10)
	    {
			bullets.splice(i,1);
		}
	}
	if(bulletDirectionControl == 4)
	{
    if(bullets[i][0] <= 620) 
		{
			bullets[i][0] += 10;
		}
		else if (bullets[i][0] >=  620)
	    {
			bullets.splice(i,1);
		}
	}
}
// Creates bullets for the bullet array based on direction
function bulletFactory()
{
	if(alive)
	{
	shoot = document.getElementById("shoot");
	shoot.play();
	var bulletDirection = direction;
	if(direction == 1) bullets.push([player_x + (player_w/3), player_y, 4, 20, bulletDirection]);
	if(direction == 2) bullets.push([player_x + (player_w/3), player_y + player_h, 4, 20, bulletDirection]);
	if(direction == 3) bullets.push([player_x, player_y + (player_h/3), 20, 4, bulletDirection]);
	if(direction == 4) bullets.push([player_x + player_w, player_y + (player_h/3), 20, 4, bulletDirection]);
    }
}
// Removes enemies from the array of enemies and pushes a blood object into the array.
function createBlood(i)
{ 
	bloodDrop.push([enemies[i][0], enemies[i][1], bloodw, bloodh]);
	enemies.splice(i,1);
	score += scoreModifier;
	enemyDead.play();
}
//Draws blood on to the canvas
function drawBlood()
{
	for (var i = 0; i < bloodDrop.length; i++)
	{
		DrawSprite (blood, bloodDrop[i][0], bloodDrop[i][1]);
	}
}
function scoreTotal(seconds, min) {
    highScoreElement.innerHTML = "High Score: " + highScore;
    scoreElement.innerHTML = "Score: " + score;
    gameTimeElapsedElement.innerHTML = "Time Elapsed: " + min + "." + seconds;
}
function displayScore()
{
    highScoreElement.style.display = 'block';
    scoreElement.style.display = 'block';
    //fpsElement.style.display = 'block';
    gameTimeElapsedElement.style.display = 'block';
}
function fitScreen(){
    arenaSize = calculateArenaSize();
   resizeElementsToFitScreen(mWidth, mHeight);
}
function getViewportSize(){
      awidth = Math.max(document.documentElement.clientWidth || window.innerWidth || 0);
      aheight = Math.max(document.documentElement.clientHeight || window.innerHeight || 0);
}

function calculateArenaSize()
{
	      getViewportSize()
          DESKTOP_ARENA_WIDTH = 800,
          DESKTOP_ARENA_HEIGHT = 520,
          arenaHeight,
          arenaWidth;
   arenaHeight = awidth*(DESKTOP_ARENA_HEIGHT/DESKTOP_ARENA_WIDTH);

      if(arenaHeight < aheight)
      {
      arenaWidth = awidth;
      }
   else
      {
      arenaHeight = aheight;
      arenaWidth = arenaHeight *(DESKTOP_ARENA_WIDTH/DESKTOP_ARENA_HEIGHT);
      }
      if(arenaWidth > DESKTOP_ARENA_WIDTH)
      {
      arenaWidth = DESKTOP_ARENA_WIDTH;
       }
   if(arenaHeight > DESKTOP_ARENA_HEIGHT)
       {
      arenaHeight = DESKTOP_ARENA_HEIGHT;
       }
   
      mWidth = arenaWidth;
      mHeight = arenaHeight;

} 


function resizeElementsToFitScreen(){
  resizeElement(document.getElementById('arena'), arenaWidth, arenaHeight);
}

  function resizeElement(element, w, h){
   element.style.width = w + 'px';
   element.style.height = h + 'px';
}

function addTouchEventHandlers() {     
canvas.addEventListener( 'touchstart', touchStart);      
canvas.addEventListener( 'touchend', touchEnd );   
}


function processBottomPad() {      
       player_y += 4;
	   direction = 2;
}
function processTopPad() {      
    player_y -= 4;
	direction = 1;
}
function processLeftPad() {      
   player_x -= 4;
   direction = 3;  
}
function processRightPad() {      
 player_x +=4;
 direction = 4;
}
function init(){
	canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d');
	opening.start();
	detectMobile();
	if(mobile){
   fitScreen();
   window.addEventListener('resize', fitScreen);
   window.addEventListener('orientationchange', fitScreen);
   addTouchEventHandlers();

              }
    else if(!mobile)
    {
	document.addEventListener('keydown', keyDown, false);
	document.addEventListener('keyup', keyUp, false);
    }
	song = document.getElementById("music");
	song.play();
	enemyCreation();
	gameTimer.start();
	gameLoop();
}

function gameLoop(){
	clearCanvas();
	ctx.drawImage(backGround,bgx, bgy);
	drawBlood();
	invulnerablity();
	if(opening.getElapsedTime() <= 5000) openingInstructions();
	gameTimeElapsed = gameTimer.getElapsedTime();
	var seconds = parseInt((gameTimeElapsed / 1000) % 60);
    var min = parseInt((gameTimeElapsed / (1000 * 60)) % 60);
	if(alive)
{
	checkIfPlayerSurvived(min);
	checkIfRoundIsOver();
	if(gameStart) enemyCreation();
	if(victory) victoryAnimation();
	if(mobile)
	{
		drawMobileIcons();
	}
    bulletCollision();
    playerCollision();
	moveEnemies();
	moveBullets();
	drawHearts();
	drawBullets();
}
    drawEnemies();
	drawplayer();
	scoreTotal(seconds, min);
	game = setTimeout(gameLoop, 1000 / 30);
}
function touchStart(e) {      
if (!victory) {         
  var preX = player_x, preY = player_y; 
    //you'll notice the two values below, client/pageX/Y get the corordinates of the current view port,
    //which is not always the same size of the canvas, and therefore are afftected by an offset 
    var widthDiff = width - awidth;
    var heightDiff = height - aheight;   
     x = e.changedTouches[0].clientX ;
     y = e.changedTouches[0].clientY  - 100 ;  
    if (alive && !victory) {  
    // chesks if the cursor is on the pad in any of the four directions
    console.log(x);
    console.log(y);   
        if (x >= 20 && x <= 79 && y >= 439 && y <= 512 ) { 
        SCREEN_HELD = setInterval(function () {
        	processLeftPad();                  
         }  , 15);  
        touchHappened = true;
         }       
         else if (x >= 151 && x <= 210 && y >= 439 && y <= 512 ) {                      	
         SCREEN_HELD = setInterval(function () {
                processRightPad(); 
            }, 15);
              touchHappened = true;        
          }   
           else if (x >= 79 && x <= 152 && y >= 380 && y <= 439 ) {
          SCREEN_HELD = setInterval(function () {
           processTopPad();  
           }, 15);  
            touchHappened = true;
       }
             else if (x >= 79 && x <= 152 && y >= 521 && y <= 580 ) {  
             SCREEN_HELD = setInterval(function () {
             	processBottomPad();
             }, 15); 
               touchHappened = true; 
            } 
            if ((preX + preY) != (player_x + player_y))
	        {
		     // if there is any difference between the x and y values from their previous states it animates the sprite
	         moving = true;
	        } 
	        // if the mouse is clicking on the gun icon this fires a bullet
	        if (x >= 434 && x >= 480 && y >= 440 && y <= 546)
	        {
              bulletFactory();
	          fireing = true, shotFired = true;
	        }  
	        if(victory || !alive)
	{
		if(x >= player_x  && x <= player_w && y >= player_y && y <= player_h) location.reload();
	} 
                      
e.preventDefault();       
}
 }
  }  

function touchEnd(e) {
 
        // Prevent players from double         
       // tapping to zoom into the canvas   
         if (!victory) {
        if (touchHappened)
        {
            clearInterval(SCREEN_HELD);
              touchHappened = false;
        }      
        e.preventDefault();       
}   
}



function keyDown(e){  // if key is pressed
	if (e.keyCode == 39) rightKey= true;
	else if (e.keyCode == 37) leftKey = true;
	if(e.keyCode == 38) upKey = true;
	else if (e.keyCode == 40) downKey = true;
	if (e.keyCode == 32 && bullets.length <= shots)
	{
    bulletFactory();
	fireing = true, shotFired = true;
	}
	if(victory || !alive)
	{
		if(e.keyCode == 83) location.reload();
	}
}
function keyUp(e) {      // if the Key is not pressed
  if (e.keyCode == 39) rightKey = false;
  else if (e.keyCode == 37) leftKey = false;
  if (e.keyCode == 38) upKey = false;
  else if (e.keyCode == 40) downKey = false;
}


window.onload = init;