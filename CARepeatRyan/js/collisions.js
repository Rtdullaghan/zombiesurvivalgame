function bulletCollision() {
 var remove = false;
	for (var i = 0; i < bullets.length; i++)
	{
		for (var j = 0; j < enemies.length; j++)
		{
			//The following gauges if the bullet's x,y values are in the range of the enemies positon and dimensions
			if (bullets[i][1] <= (enemies[j][1] + enemies[j][3])
		     && bullets[i][1] >=(enemies[j][1])
			 && bullets[i][0] >= enemies[j][0] 
			 && bullets[i][0] <= (enemies[j][0] + enemies[j][2])) 
			{
				remove = true;
				createBlood(j);
				score += 2;
			}
		}
		if(remove == true) 
		{
			//we have to remove the bullet outside of the  enemy loop but inside the bullet loop, otherwise we get an error
			bullets.splice(i,1);
			remove = false;
		}
	}
}

function playerCollision() 
{

	 var player_xw = player_x + player_w,
         player_yh = player_y + player_h;
         // the following compares the dimensions of the player to the enemy, if they collide with either side or either sprite it removes one heart and one enemy
         if (!invulnerable)
         {
  for (var i = 0; i < enemies.length; i++) 
  {
    if (player_x >= enemies[i][0] &&
        player_x <= enemies[i][0] + enemy_w &&
        player_y >= enemies[i][1] &&
        player_y <= enemies[i][1] + enemy_h) 
    { 
      hit = true;
    }
    else if (player_xw <= enemies[i][0] + enemy_w && 
    	player_xw >= enemies[i][0] &&
    	player_y >= enemies[i][1] && 
    	player_y <= enemies[i][1] + enemy_h) 
    {
      hit = true;
    }
   else  if (player_yh >= enemies[i][1] &&
        player_yh <= enemies[i][1] + enemy_h &&
        player_x >= enemies[i][0] &&
        player_x <= enemies[i][0] + enemy_w) 
    {
      hit = true;
    }
    else if (player_yh >= enemies[i][1] &&
        player_yh <= enemies[i][1] + enemy_h &&
        player_xw <= enemies[i][0] + enemy_w &&
        player_xw >= enemies[i][0])
    {
      hit = true;
    }
    if (hit)
    {
       health.splice(health.length - 1, 1);
       createBlood(i);
       playerDamage.play();
       hit = false;
    }
  }
      }
  if (health.length  == 0)
  {
  	//Game over
  	alive = false;
  }
}

