function fitScreen(){
   var arenaSize = calculateArenaSize(getViewportSize());
   resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
}

function detectMobile(){
   mobileActive = 'ontouchstart' in window;
}

function getViewportSize() {
   return{
      width: Math.max(document.documentElement.clientWidth || window.innerWidth || 0),
      height: Math.max(document.documentElement.clientHeight || window.innerHeight || 0)
   };
}

function resizeElement(element, w, h) {
    element.style.width = width + "px";
    element.style.height = height + "px";
};

function resizeElementsToFitScreen(arenaWidth, arenaHeight) {
    resizeElement(document.getElementById('arena'), arenaWidth, arenaHeight);
    resizeElement(mobileWelcomeToast, arenaWidth, arenaHeight);
    resizeElement(mobileStartToast, arenaWidth, arenaHeight);
};
function calculateArenaSize(viewportSize) {
    var DESKTOP_ARENA_WIDTH = 800,
            DESKTOP_ARENA_HEIGHT = 400;


    arenaHeight = viewportSize.width * (DESKTOP_ARENA_HEIGHT / DESKTOP_ARENA_WIDTH);
    if (arenaHeight < viewportSize.height) {
        arenaWidth = viewportSize.width;
    } else {
        arenaHeight = viewportSize.height;
        arenaWidth = arenaHeight * (DESKTOP_ARENA_WIDTH / DESKTOP_ARENA_HEIGHT);
    }
    if (arenaWidth > DESKTOP_ARENA_WIDTH) {
        arenaWidth = DESKTOP_ARENA_WIDTH;
    }
    if (arenaHeight > DESKTOP_ARENA_HEIGHT) {
        arenaHeight = DESKTOP_ARENA_HEIGHT;
    }
    return{
        width: arenaWidth,
        height: arenaHeight
    };
}
function touchStart(e) {
    var x = e.changedTouches[0].pageX;  //x position of touch
    var y = e.changedTouches[0].pageY; //y position of touch
    if (gameStarted) {
        if (x > screen.width / 2)
        {
            
            processRightTap();// to shoot            
        } else if (x < screen.width / 2 && y < screen.height / 2)
        {
            SCREEN_HELD = setInterval(function () {
                processTopLeftTap();
            }, 15);//sets interval until touch ends

        } else if (x < screen.width / 2 && y > screen.height / 2)
        {
            SCREEN_HELD = setInterval(function () {
                processBottomLeftTap();
            }, 15);

        }
// Prevent players from double         
// tapping to zoom into the canvas         
        e.preventDefault();
    }
};

function touchEnd(e) {
    var x = e.changedTouches[0].pageX;

    if (gameStarted) {
        if (x < screen.width / 2)
        {
            clearInterval(SCREEN_HELD);
        }

// Prevent players from double         
// tapping to zoom into the canvas         
        e.preventDefault();
    }
};
function processRightTap()
{
    bullets.push(new Bullet(ship.x + (shSprite.w * .9), ship.y + (shSprite.h / 2), -8, 12, 4, "#FFFF00"));
        if(SOUND)
        {
        shootSound = document.getElementById("shoot");
        shootSound.play();
        }
}
;

function processBottomLeftTap() {

    ship.y += 4;
    ship.y = Math.max(Math.min(ship.y, screen.height - ((shSprite.h / 2) + 35)), -50);
}

function processTopLeftTap() {

    ship.y -= 4;
    ship.y = Math.max(Math.min(ship.y, screen.height - ((shSprite.h / 2) + 35)), -50);
}
