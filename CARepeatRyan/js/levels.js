
 function saveHighscore()
{
    if (score > highScore || highScore === null)
    {
        highScore = score;
        localStorage.setItem("HighScore", score);
    }
};
 function writeOnScreen(text, size, col, xModifier, yModifier)
 {
  ctx.font = 'bold ' + size + 'px Arial';
  ctx.fillStyle = col;
  ctx.fillText(text, xModifier, yModifier);
 }
function checkIfRoundIsOver()
{
	if (!victory && alive)
	{
	if(enemies.length == 0)
	{
		if(roundWillEnd)
		{
      roundTimer.start();
      roundWillEnd = false;
        }
        if(roundTimer.getElapsedTime() > 5000) 
        	{
        		speed++;
        		scoreModifier +=2;
        		enemyTotal += 2;
        		enemyCreation();
	            transition.play();
        	}
        	if(roundTimer.getElapsedTime() >= 1000 && roundTimer.getElapsedTime()  < 2000) writeOnScreen("Well Done!", 42, '#fff', 300, height/2);
        	if(roundTimer.getElapsedTime() >= 2000 && roundTimer.getElapsedTime()  < 3000) writeOnScreen("One!", 60, '#f00', 300, height/2);
        	if(roundTimer.getElapsedTime() >= 3000 && roundTimer.getElapsedTime()  < 4000) writeOnScreen("Two!", 78,'#fff', 300, height/2);
            if(roundTimer.getElapsedTime() >= 4000 && roundTimer.getElapsedTime()  < 5000) writeOnScreen("Three!", 96, '#f00', 300, height/2);
      
	}
}
}
function openingInstructions()
{
	if(!mobile){
	        writeOnScreen("Survive 2 minutes", 30, 'red', 200, 200);
            writeOnScreen("Kill All Mutants", 30, 'red', 200, 250);
        	writeOnScreen("Press Space to Shoot", 30,'red', 200, 300);
            writeOnScreen("Direction Keys to move ", 30, 'red', 200, 350);
            writeOnScreen("Restart with S", 30, 'red', 200, 400);
        }
        else if(mobile)
        {
        	writeOnScreen("Survive 2 minutes", 30, 'red', 200, 200);
            writeOnScreen("Kill All Mutants", 30, 'red', 200, 250);
        	writeOnScreen("Use Gun to Shoot", 30,'red', 200, 300);
            writeOnScreen("D-Pad to move ", 30, 'red', 200, 350);
            writeOnScreen("wait to restart", 30, 'red', 200, 400);
        }
}

function checkIfPlayerSurvived(min)
{
	if (min === 2 && !victory)
	{
		victory = true;
		enemyTotal += 20;
		enemyCreation();
		saveHighscore();
		invulnerable = true;
		victoryStart = true;
		invulnerableDuration = 6000;
	}

}
// spawns several enemies then after three seconds the enemies die, indicating the end of the Mutant ending
function victoryAnimation()
{ 
		if(victoryAnimationTimer.getElapsedTime() === undefined)
	   {
	   victoryAnimationTimer.start();
	   victoryStart = false;
	   }
	   if (victoryAnimationTimer.getElapsedTime() > 3000)
	   {
		for(var i = 0; i < enemies.length; i++)
		{
			createBlood(i);
			score -= scoreModifier;

		}
	    }
	 if (victoryAnimationTimer != undefined) writeOnScreen("You survived!", 30, '#f00', 200, 200);
	 if (victoryAnimationTimer != undefined) writeOnScreen("You Slayed Mutants!", 30, '#f00', 200, 250);
	 if (victoryAnimationTimer != undefined) writeOnScreen("Humanity Lives", 30, '#f00', 200, 300);
	 if (victoryAnimationTimer != undefined) writeOnScreen("Another Day!", 30, '#f00', 200, 350);
}
//Gives the player Two seconds when enemies first spawn to reposition, within these two seonds the player cannot collide with enemies.
function invulnerablity()
{
	if(invulnerableStart)
	{
		invulnerablityTimer.start();
		invulnerable = true;
		invulnerableStart = false;
	}
	if (invulnerablityTimer.getElapsedTime() >= invulnerableDuration)
	{
		invulnerable = false;
		invulnerableDuration = 2000;
	}
}